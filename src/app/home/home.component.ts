import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('container') container: ElementRef;
  user :string;
  
  ngOnInit(): void {
  }

  constructor(private authService: AuthService) {
    this.user = this.authService.currentUserValue();
    this.loadScripts();
   }

   ngAfterViewInit() {
    if(this.user!==null) {
      const ngEl = document.createElement('app-employeeprofile');
      ngEl.setAttribute('editable', "true");
      ngEl.setAttribute('empid',this.user.toString());
      this.container.nativeElement.appendChild(ngEl);
    }
   
  }

  loadScripts() {
    const styleTag = document.createElement('link');
    styleTag.rel = "stylesheet";
    styleTag.href = 'http://127.0.0.1:8081/styles.css';
    document.getElementsByTagName('body')[0].appendChild(styleTag);
    const externalScriptArray = [
      'https://cdnjs.cloudflare.com/ajax/libs/zone.js/0.9.1/zone.min.js',
      'https://cdnjs.cloudflare.com/ajax/libs/webcomponentsjs/2.2.10/custom-elements-es5-adapter.js',
      'http://127.0.0.1:8081/main.js'
    ];
    for (let i = 0; i < externalScriptArray.length; i++) {
      const scriptTag = document.createElement('script');
      scriptTag.src = externalScriptArray[i];
      scriptTag.type = 'text/javascript';
      scriptTag.async = false;
      scriptTag.charset = 'utf-8';
      document.getElementsByTagName('body')[0].appendChild(scriptTag);
    }
  }

}
