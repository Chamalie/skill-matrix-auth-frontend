import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  // loadScripts() {
  //   const styleTag = document.createElement('link');
  //   styleTag.rel = "stylesheet";
  //   styleTag.href = 'http://127.0.0.1:8081/styles.css';
  //   document.getElementsByTagName('body')[0].appendChild(styleTag);
  //   const externalScriptArray = [
  //     'https://cdnjs.cloudflare.com/ajax/libs/zone.js/0.9.1/zone.min.js',
  //     'https://cdnjs.cloudflare.com/ajax/libs/webcomponentsjs/2.2.10/custom-elements-es5-adapter.js',
  //     'http://127.0.0.1:8081/main.js'
  //   ];
  //   for (let i = 0; i < externalScriptArray.length; i++) {
  //     const scriptTag = document.createElement('script');
  //     scriptTag.src = externalScriptArray[i];
  //     scriptTag.type = 'text/javascript';
  //     scriptTag.async = false;
  //     scriptTag.charset = 'utf-8';
  //     document.getElementsByTagName('body')[0].appendChild(scriptTag);
  //   }
  // }

}
