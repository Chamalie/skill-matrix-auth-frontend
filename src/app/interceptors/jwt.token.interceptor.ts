// import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
// import { AuthService } from '../services/auth.service';
// import { Observable, of, throwError } from 'rxjs';
// import { Injectable } from '@angular/core';
// import { catchError } from 'rxjs/operators'; 
// import { Router } from '@angular/router';

// @Injectable()
// export class JwtTokenInterceptor implements HttpInterceptor {
 
//     constructor(public auth: AuthService, public router: Router) {}
   
//     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//       let interceptedRequest = request.clone({
//         setHeaders: {
//           Authorization: `Bearer ${this.auth.getToken()}`
//         }
//       });
   
//       return next.handle(interceptedRequest).pipe(catchError(x => this.handleErrors(x)));
//     } 

//     handleErrors(err: HttpErrorResponse): Observable<any>{
//         if(err.status==401) {
//           console.log("caught error")
//             this.auth.redirectToUrl = this.router.url;
//             this.auth.logout();
//             // this.router.navigate(['skillMatrix/login']);
//             return throwError(err);
//         }
//         return throwError(err);
//     }
// }