import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class RouteGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {} 
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean{
      const allowedRoles = next.data.allowedRoles;
      const isAuthorized = this.authService.isAuthorized(allowedRoles);
    
    if (!isAuthorized) {
        this.router.navigate(['skillMatrix/home']);
      }
    
    return isAuthorized;
  
  }
  
}
