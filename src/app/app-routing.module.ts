import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './guards/auth.guard';
import { Role } from './role';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouteGuard } from './guards/route.guard';

const routes: Routes = [ { path: '', pathMatch: 'full', redirectTo: 'skillMatrix/login' },
{ 
  path: 'skillMatrix/login', 
  component: LoginComponent 
},
{ 
  canActivate: [AuthGuard], 
  path: 'skillMatrix/home', 
  component: HomeComponent, 
  runGuardsAndResolvers: 'always'
  
},
{
  canActivate: [AuthGuard, RouteGuard], 
  path: 'skillMatrix/dashboard', 
  component: DashboardComponent, 
  runGuardsAndResolvers: 'always',
  data: {
    allowedRoles: [Role.Manager]
  }
}
];
 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
