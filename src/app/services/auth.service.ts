import { Injectable } from '@angular/core';
import { TokenService } from './token.service';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Role } from '../role';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  static readonly TOKEN_STORAGE_KEY;
  redirectToUrl: string = 'skillMatrix/home';
  isLoginFailed: boolean = false;
  // loginError: string;
  loginError: Subject<String> = new BehaviorSubject<String>(null);
  
  constructor(private router: Router, private tokenService: TokenService, 
    private jwtHelperService: JwtHelperService) {
   }

  //get user id
  public currentUserValue(): string {
    //return this.currentUserSubject.value;
    return localStorage.getItem('currentUser');
  }

  //save user id
  public saveUserValue(user:any) {
    localStorage.setItem('currentUser', JSON.stringify(user));
  }

  //save jwt
  private saveToken(token: string){
    token = token.substring(7);
    localStorage.setItem(AuthService.TOKEN_STORAGE_KEY, token);
    console.log(token);
  }
  
  //get jwt
  public getToken(): string {
    return localStorage.getItem(AuthService.TOKEN_STORAGE_KEY);
  }

  //login 
  public login(credentials: FormGroup): any {
    this.tokenService.getResponseHeaders(credentials)
    .subscribe((data) => {
      this.saveToken(data.headers.get('Authorization'));
      this.saveUserValue(data.body);
      this.router.navigate(["skillMatrix/home"]);
      return this.getToken;
    },
    (err:HttpErrorResponse) => {
      this.isLoginFailed = true;
      this.loginError.next(err.error.message);
      return this.loginError;
    }
    );  
  }

  //logout
  public logout(): void {
      localStorage.removeItem(AuthService.TOKEN_STORAGE_KEY);
      localStorage.removeItem("currentUser");
  }
  
  //return login status
  public isLoggedIn(): boolean {
      return !!this.getToken();
  }

  //check if the user is authorized to follow the route
  isAuthorized(allowedRoles: string[]): boolean {
    // check if the list of allowed roles is empty, if empty, authorize the user to access the page
    if (allowedRoles == null || allowedRoles.length === 0) {
      return true;
    }
    const token = this.getToken();
    const decodeToken = this.jwtHelperService.decodeToken(token);
    if (!decodeToken) {
      return false;
    }
    var roles= decodeToken['authorities'];
    return allowedRoles.includes(roles[0]['authority']);
  }

  //check if the user is a manager or not
  isManager() {
    const token = this.getToken();
    const decodeToken = this.jwtHelperService.decodeToken(token);
    if (!decodeToken) {
      return false;
    }
    var roles= decodeToken['authorities'];
    return (Role.Manager === roles[0]['authority']);
  }
}
