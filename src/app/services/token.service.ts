import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  
  api_url: string = "http://localhost:8080";
  
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'}), observe:'response' as 'response'
  };
  
  constructor(private http:HttpClient) { }

  public getResponseHeaders(credentials: FormGroup) : Observable<any> {
    localStorage.clear();
    return this.http.post(this.api_url+"/login", {
      username: credentials.get("username").value,
      password: credentials.get("password").value
    }, this.httpOptions);
  }

}
