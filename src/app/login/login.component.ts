import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthService} from '../services/auth.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage: String;

  constructor(private authService: AuthService, private router: Router) { 
  }

  ngOnInit(): void {
    this.authService.loginError.subscribe((loginErr) => { this.errorMessage = loginErr });    
  }

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('',[Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  onSubmit() {
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm);
      this.isLoginFailed = this.authService.isLoginFailed;
      // this.errorMessage = this.authService.loginError;
     }
  }

}
